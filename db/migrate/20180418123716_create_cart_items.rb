class CreateCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_items do |t|
      t.references :cart, index: true, foreign_key: true
      t.references :train_class, index: true, foreign_key: true
      t.references :passenger, index: true, foreign_key: true
      
      t.integer :passenger_qty
      t.integer :male
      t.integer :female
      t.decimal :unit_price
      t.decimal :total_price
      t.string  :outbound
      t.string  :order_state
      
      t.timestamps
    end
  end
end
