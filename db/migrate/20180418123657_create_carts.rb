class CreateCarts < ActiveRecord::Migration[5.2]
  def change
    create_table :carts do |t|
      
      t.string :status
      t.string :delivery_status
      t.decimal :subtotal
      t.string  :uuid,unique: true
      t.timestamps
    end
  end
end
