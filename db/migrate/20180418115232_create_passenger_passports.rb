class CreatePassengerPassports < ActiveRecord::Migration[5.2]
  def change
    create_table :passenger_passports do |t|
      t.integer :cart_item_id
      t.string :number
      # t.integer :male
      # t.integer :female
      t.string :book_ref
      t.timestamps
    end
  end
end
