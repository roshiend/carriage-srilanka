class CreateTrainClasses < ActiveRecord::Migration[5.2]
  def change
    create_table :train_classes do |t|
      t.string :name
      t.decimal :price
      t.integer :train_id
      t.timestamps
    end
  end
end
