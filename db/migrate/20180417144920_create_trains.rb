class CreateTrains < ActiveRecord::Migration[5.2]
  def change
    create_table :trains do |t|
      t.time :arrival_time
      t.time :depature_time
      t.string :destination
      t.time :destination_time
      t.time :end_station_time
      t.string :frequency
      t.string :train_type
      t.string :train_number
      t.string :train_name
      #t.integer :train_class_id
      
      t.integer :start_station_id #(depatures/ arrivals)
      t.integer :end_station_id #(station where u wants to go)
      t.timestamps
    end
  end
end
