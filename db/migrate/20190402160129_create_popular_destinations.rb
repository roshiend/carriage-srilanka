class CreatePopularDestinations < ActiveRecord::Migration[5.2]
  def change
    create_table :popular_destinations do |t|
      t.string:destination_name
      t.integer :rating
      t.timestamps
    end
  end
end
