# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Station.delete_all
Station.create! id: 1, name: "Badulla", station_code: "bad"
Station.create! id: 2, name: "Colombo", station_code: "fot"
Station.create! id: 3, name: "Galle", station_code: "gle"
Station.create! id: 4, name: "Anuradhapura", station_code: "anp"
Station.create! id: 5, name: "Nuwara Eliya", station_code: "nue"
Station.create! id: 6, name: "Kandy", station_code: "kdt"
Station.create! id: 7, name: "Ella", station_code: "ell"


Train.delete_all
Train.create! id: 1, arrival_time: "2017-10-25 04:04:00.000000", depature_time: "2017-10-25 04:04:00.000000", destination:"Ella",
              destination_time:"2017-10-25 10:00:00.000000", end_station_time: "2017-10-25 09:09:00.000000", 
              frequency:"DAILY", train_type:"EXPRESS", train_number:"10020",train_name:"PODI",start_station_id: "2", end_station_id:"7"

Train.create! id: 2, arrival_time: "2017-10-27 04:10:00.000000", depature_time: "2017-10-27 04:04:00.000000", destination:"Badulla",
              destination_time:"2017-10-29 10:00:00.000000", end_station_time: "2017-10-29 09:09:00.000000", 
              frequency:"DAILY", train_type:"EXPRESS", train_number:"10020",train_name:"PODI",start_station_id: "2", end_station_id:"1"
              
Train.create! id: 3, arrival_time: "2017-10-27 04:10:00.000000", depature_time: "2017-10-27 04:04:00.000000", destination:"colombo",
              destination_time:"2017-10-29 10:00:00.000000", end_station_time: "2017-10-29 09:09:00.000000", 
              frequency:"DAILY", train_type:"EXPRESS", train_number:"10020",train_name:"PODI",start_station_id: "7", end_station_id:"1"
              
Train.create! id: 4, arrival_time: "2017-10-27 04:10:00.000000", depature_time: "2017-10-27 04:04:00.000000", destination:"colombo",
              destination_time:"2017-10-29 10:00:00.000000", end_station_time: "2017-10-29 09:09:00.000000", 
              frequency:"DAILY", train_type:"EXPRESS", train_number:"10020",train_name:"PODI",start_station_id: "1", end_station_id:"2"
              
Train.create! id: 5, arrival_time: "2017-10-27 04:10:00.000000", depature_time: "2017-10-27 04:04:00.000000", destination:"colombo",
              destination_time:"2017-10-29 13:00:00.000000", end_station_time: "2017-10-29 23:09:00.000000", 
              frequency:"DAILY", train_type:"EXPRESS", train_number:"10020",train_name:"PODI",start_station_id: "1", end_station_id:"2"
              

              
                
TrainClass.delete_all
TrainClass.create! id: 1, name: "standard", price:"12.50", train_id: "1"
TrainClass.create! id: 2, name: "Second", price:"15.50", train_id: "1"
TrainClass.create! id: 3, name: "first", price:"21.50", train_id: "1"

TrainClass.create! id: 4, name: "standard", price:"12.50", train_id: "2"
TrainClass.create! id: 5, name: "Second", price:"15.50", train_id: "2"
TrainClass.create! id: 6, name: "first", price:"21.50", train_id: "2"

TrainClass.create! id: 7, name: "standard", price:"16.10", train_id: "3"
TrainClass.create! id: 8, name: "Second", price:"18.00", train_id: "3"
TrainClass.create! id: 9, name: "first", price:"23.50", train_id: "3"

TrainClass.create! id: 10, name: "standard", price:"16.10", train_id: "4"
TrainClass.create! id: 11, name: "Second", price:"18.00", train_id: "4"
TrainClass.create! id: 12, name: "first", price:"23.50", train_id: "4"
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.production?