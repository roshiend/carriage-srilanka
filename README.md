# README

# DEMO
https://carriage-lanka.herokuapp.com/


Advance tickets booking application for forigners who visit sri lanka

Travelers can book specific train tickets before they travel Sri Lanka through the website
	They can purchase tickets by providing their passport number as booking reference issue to the pass port number.
	Travelers can collect train tickets from airport by providing the refrence number. 
	Max tickets for one passport is 5. 


versions
	Rails 5 with psql
	ruby 2.6.1

Intregrations
	Braintree Paypal intergration
	Sendgrid 

features
	Omniauth (removed as client dosent want it)
	Automatic email/confirmation from backend via sendgrid
	Error handling

Internationalization	
	Chinese/English translations

Backend Dashboard
	Activeadmin 


# Addresed bugs	
	anyone could change date params in url and do the purchase and fixed by trackig the min date value  
		DateCheckerHelper

	auto calculate & set male and female count based on passenger_qty param 
		reservations.js

