class OrderAlertAdminMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.receipt_mailer.purchase_order.subject
  #
  def purchase_notification (opts={})
   
    # default from: "Invoice<purchases@SrilankaTrainticketsbooking.com>"
    mail(:to => "roshiend@gmail.com", :from => 'PurchaseNotification@Carrege.lk',:subject => 'New Purchase')   
  end
end
