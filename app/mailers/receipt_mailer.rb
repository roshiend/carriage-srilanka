class ReceiptMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.receipt_mailer.purchase_order.subject
  #
  def purchase_order (passenger, current_order, opts={})
    @current_order = current_order
    @passenger = passenger
    @purchased_items = @current_order.cart_items
    # default from: "Invoice<purchases@SrilankaTrainticketsbooking.com>"
    mail(:to => passenger.email, :from => 'Confirmation<purchases@srilankaonlinetrainticketsbooking.com>',:subject => 'Your Purchase Order')   
  end
end
