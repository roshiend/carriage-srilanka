class BookingConfirmation < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.booking_confirmation.deleivery_mail.subject
  #
  def deleivery_mail(cart_item, opts={} )
    @cart_item = cart_item
    @passenger = @cart_item.passenger
    #email = @passenger.email
    
    mail(to: @passenger.email, from: 'Bookings<confirmations@ticketrails.com>',
        subject: 'Booking Confirmation')
   
   
  end
end
