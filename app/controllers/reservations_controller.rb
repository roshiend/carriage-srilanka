class ReservationsController < ApplicationController
  before_action :authenticate_passenger!
  include DateCheckerHelper
  
  def show
    @train_class = TrainClass.find(params[:train_class_id])
    @travelers = [0,1,2,3,4,5,6,7,8,9,10]
    @travelers_select = params[:travelers]
    
    
    @cart_item = current_order.cart_items.new
    if date_valid
      redirect_to root_path, alert: "Invalid Date"
    end 
    
    
  end
end
