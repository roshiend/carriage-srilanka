class PurchasesController < ApplicationController
  before_action :authenticate_passenger!
  def show
      @my_cart =current_passenger.cart_items
      @my_purchases = @my_cart.joins(:cart).where('carts.status'=> "purchased").paginate(page: params[:page], per_page: 3)
  
  end
  
  def all_orders
    @my_cart =current_passenger.cart_items
    @my_purchases = @my_cart.joins(:cart).where('carts.status'=> "purchased").paginate(page: params[:page], per_page: 3)
    render action: :show
    
  end  
  
  def open_orders
    @my_cart =current_passenger.cart_items
    @my_purchases = @my_cart.joins(:cart).where('carts.status'=> "purchased", "order_state" => "placed").paginate(page: params[:page], per_page: 3)
    render action: :show
  end
  
  def completed_orders
    @my_cart =current_passenger.cart_items
    @my_purchases = @my_cart.joins(:cart).where('carts.status'=> "purchased", "order_state" => "Deliverd").paginate(page: params[:page], per_page: 3)
    render action: :show
  end  
end
