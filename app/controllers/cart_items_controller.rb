class CartItemsController < ApplicationController
    def create
    @cart = current_order
    @cart_item = @cart.cart_items.new(cart_item_params)
    
    @cart.save
    
    # if @cart.save
    #   puts "saved #{@cart_item.id}"
    # else
    #   puts "didnt save #{@cart_item.id}"
    # end  
    respond_to do |format|
      if @cart.save
        format.html
        format.js{redirect_to cart_path}
        
     
      end
    
    end
      session[:cart_id] = @cart.id
    end
  
   
  

  def destroy
    @cart = current_order
    @cart_item = @cart.cart_items.find(params[:id])
    @cart_item.destroy
    @cart_items = @cart.cart_items
    
    respond_to do |format|
     
        format.html
        format.js 
     
    end
    
  end
  
  private 
    def cart_item_params
      params.require(:cart_item).permit(:passenger_qty, :passenger_id,:train_class_id, :cart_id, 
      :outbound,:order_state,:male,:female,passenger_passports_attributes: [ :cart_item_id, :number, :passenger_qty, :outbound, :book_ref])
    end
    
    
end
