class CartsController < ApplicationController
  skip_before_action :verify_authenticity_token
    def show
        @cart_items = current_order.cart_items
        @cart_item = current_order.cart_items.find_by(params[:id])
    end
    
    def payment
      # @cart_items = current_order.cart_items
    Cart.find(session[:cart_id])
    result = Braintree::Transaction.sale(
              amount: current_order.subtotal,
              payment_method_nonce: params[:payment_method_nonce],
              
              :options => {
                :submit_for_settlement => true},
             
                  
              )
    response = {:success => result.success?}          
    if result.success?
      #puts "Thank you for your purchase, success!: #{result.transaction.id}"
      response[:transaction_id] = result.transaction.id
      current_order.update(status: "purchased")
      
      ReceiptMailer.purchase_order(current_passenger, current_order).deliver_now # sending proof of purchase email to customer
       
       OrderAlertAdminMailer.purchase_notification.deliver_now 
       
      redirect_to root_path, notice: "Thank you for booking, Please check your email for invoice"
      session.delete(:cart_id)
      
    elsif result.transaction
      puts "Something went wrong while processing your transaction.Payment was not sucessful!. Please try again!"
      puts "code: #{result.transaction.processor_response_code}"
      puts "code: #{result.transaction.processor_response_text}"
      error_messages = result.errors.map { |error| "Error: #{error.code}: #{error.message}" }
      # flash[:error] = error_messages
      redirect_to cart_path, alert: "something went wrong, your transcations was not successful!"
      
      
      response[:error] = result.transaction.processor_response_text
      
    else
      p result.errors
      
      response[:error] = result.errors.inspect
      redirect_to cart_path, alert: result.errors.map { |error| "#{error.message}" }
    end
     #render :json => response  
    end    
        
end
