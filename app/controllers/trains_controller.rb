class TrainsController < ApplicationController
  include DateCheckerHelper
  def home
    @train = Train.new
    @stations = Station.all.map { |station| [station.name, station.id] }
    # @travle_day = params[:traveling]
    @passengers = [1,2,3,4,5,6,7,8,9,10]
    
    @destinations = PopularDestination.all
    
    
  end

  def result
    
    
    if params[:train].nil?
      render home
    end
    
    if !params[:train].nil?
      @from = params[:train][:from_station]
      @to = params[:train][:to_station]
     
    # @travel_day_select = params[:train][:traveling]
      @passengers_select = params[:train][:passengers]
      
     
      @trains = Train.search(@from, @to)

      respond_to do |format|
        format.html
        format.js
      end
    end 
    
    
   @train = @trains.find_by(params[:id])
    
    
    
    if date_valid
      redirect_to root_path, alert: "Invalid Date"
      puts "manually changed to invalid date - should return to home page"
    end 
    
    
    
    
  end
end
