class ApplicationController < ActionController::Base
  rescue_from ArgumentError, :with => :argument_error_rescue ## error handler redirection on 404 and 500
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found ## error handler redirection on 404 and 500
  before_action :set_cache_buster
  helper_method :current_order 
  before_action :set_locale 
  private

    def argument_error_rescue
      redirect_to root_path
    end 
    
    def record_not_found
      redirect_to root_path
    end
    
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def self.default_url_options(options={})
    options.merge({ :locale => I18n.locale })
  end
  
  
  before_action :configure_permitted_parameters, if: :devise_controller? # devise adding custome fields
  
  def configure_permitted_parameters
    added_attrs = [:first_name,:second_name, :email, :password, :password_confirmation, :remember_me]
    update_attrs = [:email, :password, :password_confirmation, :remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: update_attrs
  end
  
  
  # clear cache memory in redicrection to home page
    def set_cache_buster
      response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
      response.headers["Pragma"] = "no-cache"
      response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
    end
  
  
   
  def current_order 
    if !session[:cart_id].nil?
     
      
      Cart.find(session[:cart_id])
    else
     
    Cart.new
    end
  end  
  
  
end
