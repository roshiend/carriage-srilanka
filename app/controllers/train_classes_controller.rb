class TrainClassesController < ApplicationController
  include DateCheckerHelper
  
  
  def show
    @train = Train.find(params[:train_id])
   
    
    if date_valid
      redirect_to root_path, alert: "Invalid Date"
    end 
  end
end
