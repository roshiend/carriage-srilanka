module ApplicationHelper
    def pounds(gbp)
      number_to_currency(gbp, :unit => "£ " )
    end
end
