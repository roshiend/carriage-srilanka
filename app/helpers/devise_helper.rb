module DeviseHelper
  def devise_error_messages!
    return '' if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    html = <<-HTML
    <div id="error_wrapper">
        <i class="fa fa-exclamation-circle notice-error" aria-hidden="true"></i>
        <strong>
       #{pluralize(resource.errors.count, "error")} must be fixed
      </strong>
      #{messages}
    </div>
    HTML

    html.html_safe
  end
end