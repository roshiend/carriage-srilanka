class PopularDestination < ApplicationRecord
    has_one_attached :picture
    
    before_save :add_rating
    validates :picture, attached: true, content_type: ['image/png', 'image/jpg', 'image/jpeg']
    
    private
        def add_rating
            self.rating = 5
        end
    
    def thumb
        return self.picture.variant(resize: '300x300!').processed
    end
end
