class TrainClass < ApplicationRecord
    before_save :titleize_train_class
    belongs_to :train
  has_many :cart_items 
  
  private
    def titleize_train_class
        name.titleize
        
    end    
  
end
