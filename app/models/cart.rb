class Cart < ApplicationRecord
  require 'securerandom'  

  has_many :cart_items
  before_create :set_cart_status
  before_create :set_uuid 
  
  before_save :update_subtotal
  
  #after_update :email_purchase

  def subtotal
    cart_items.collect { |oi| oi.valid? ? (oi.passenger_qty * oi.unit_price) : 0 }.sum
  end
  
  # def email_purchase
  #   if status_changed? && status == "purchased"
  #     ReceiptMailer.purchase_order(self).deliver_now
      
      
  #   end
  #end  

  
private
  def set_cart_status
    self.status = "pending"
    self.delivery_status = "placed"
    
  end

  def update_subtotal
    self[:subtotal] = subtotal
  end
  
  def set_uuid
    self.uuid = SecureRandom.hex(3)
  end  
  
end
