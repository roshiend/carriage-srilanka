class PassengerPassport < ApplicationRecord
    belongs_to :cart_item
     validates_presence_of :number
  
  before_create :set_booking_ref

  
  private
  def set_booking_ref
      self.book_ref = "Enter_Reference"
  end   
  
end
