class Train < ApplicationRecord
 validates :start_station_id, :end_station_id,:arrival_time, :depature_time,
            :destination,:destination_time,:end_station_time, :frequency, :train_type,
            :train_number, :train_name, presence: true
            
            
    belongs_to :from_station, class_name: 'Station', foreign_key: 'start_station_id'
  belongs_to :to_station, class_name: 'Station', foreign_key: 'end_station_id'
  
  has_many :train_classes, :dependent => :destroy
    accepts_nested_attributes_for :train_classes, :allow_destroy => true 
    
    
  before_save :titleize_all
  
  
  def self.search(from, to)
    where(start_station_id: from, end_station_id: to)
  end
  
  
  private 
    
  def titleize_all
        
    
    destination.titleize 
    frequency.titleize 
    train_type.titleize 
    train_name.titleize 
     
  end    
end
