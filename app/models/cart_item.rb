class CartItem < ApplicationRecord
  belongs_to :cart
  belongs_to :train_class
  belongs_to :passenger
  
  
  after_update :send__order_deleivery_mail
  
  has_many :passenger_passports, :dependent => :destroy
    accepts_nested_attributes_for :passenger_passports, allow_destroy: true
  
  
  
  validates :passenger_qty, presence: true, numericality: { only_integer: true, min: 0,  max: 10 }
  
  validate :passenger_qty_size
  
  validate :train_class_present
  validate :cart_present
  
  before_save :check_for_nil_male_female
  before_save :finalize
  before_create :set_order_state
 
  
  scope :CompletedBookings, ->{joins(:cart).where('carts.status'=> "purchased", "order_state" => "Delivered")}
  scope :PendingBookings, ->{joins(:cart).where('carts.status'=> "purchased", "order_state" => "placed")}
  
  
  
  def unit_price
    if persisted?
      self[:unit_price]
    else
      train_class.price
    end
  end

  def total_price
    unit_price * passenger_qty
  end
  
  def send__order_deleivery_mail
    if order_state_changed? && order_state == "Deliverd"
      BookingConfirmation.deleivery_mail(self).deliver_now
        #ReceiptMailer.deleivery_mail(cart_item,passenger).deliver_now
    end  
  end 

private

  def passenger_qty_size
    errors.add(:passenger_qty, "exceed the passengers limit") if passenger_qty >10
  end  
  
  def train_class_present
    if train_class.nil?
      errors.add(:train_class, "is not valid or is not active.")
    end
  end

  def cart_present
    if cart.nil?
      errors.add(:cart, "is not a valid cart.")
    end
  end

  def finalize
    self[:unit_price] = unit_price
    self[:total_price] = passenger_qty * self[:unit_price]
  end
  
  
  def set_order_state
    self.order_state = "placed"
  end 
  
  
  def check_for_nil_male_female
    if self.male.nil?
      self.male = 0
    end
    
    if self.female.nil?
      self.female = 0
    end  
  end  
  
end
