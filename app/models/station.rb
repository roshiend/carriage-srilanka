class Station < ApplicationRecord
  before_save :titleize_name
  validate :name, :station_code
  
  has_many :depatured_trains, class_name: 'Train' , foreign_key: 'start_station_id'
  has_many :arriving_trains, class_name: 'Train' , foreign_key: 'end_station_id'
  
  private 
    
  def titleize_name
    name.titleize  
    station_code.upcase!
  end  
end
