/* global $ i female male i*/  


$(document).ready(function() {

function getQueryVariable(variable) //  split and find url param value 
{
      var query = window.location.search.substring(1);
      var vars = query.split("&");
      for (var i=0;i<vars.length;i++) {
              var pair = vars[i].split("=");
              if(pair[0] == variable){return pair[1];}
      }
      return(false);
}

var MaxValue = getQueryVariable("passenger_qty"); // getting url splited value and pass its value 
var MaxIntValue = parseInt(MaxValue);

//disable add to cart button if gender selection value is zero
if(female.value == 0 && male.value == 0){
  	document.getElementById("add_to_crt_btn").disabled = true;
}

//  auto calculation depends on selection
  $('select').change(function(){  
    var CurValue = $(this).val();
    var NewValue = MaxIntValue - CurValue;
    if ($(this).is('#female')) {
        $('#male').val(NewValue);
    } else {
        $('#female').val(NewValue);      
    } 
    
    if (female.value > MaxIntValue){
    	female.value = MaxIntValue;
      male.value = 0;
   
    }
    if(male.value > MaxIntValue ){
    male.value = MaxIntValue;
      female.value = 0;
    }
    
    if((female.value !== 0 && male.value !== 0)){
   		document.getElementById("add_to_crt_btn").disabled = false; // enable true depends on button slection
  	}
  });   

});