/*global $*/

//set datpicker format and min date
$( function() {
    $( ".datepicker" ).datepicker({dateFormat: "dd/mm/yy", minDate: 1});
} );
