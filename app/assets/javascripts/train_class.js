/*global $*/


// disable button and enable upon click of radio buttons
$(document).ready(function() {
    document.getElementById("order-btn").disabled = true;
   $('input:radio[name=train_class_id]').click(function () {
     
     document.getElementById("order-btn").disabled = false;
   });
});
