/*global $ location*/

// animated close bar button form mobile view
 $(document).on('turbolinks:load',function() {
   
 // dropdown nav button click   
     $('.fa-ellipsis-v').click(function() {
        $(".log_wish_etc_con").toggleClass('drop_me');
    });
 //dropdown close button click
    $('.fa-times').click(function() {
       $(".log_wish_etc_con").toggleClass('drop_me');
       
    });
    
 //mobile side nav animated  button click
    $('._anim_bar').click(function() {
        $(this).toggleClass('change');
        $("#mob_nav").toggleClass('slide_me');
    });
    
     
 //nackbar type flash messages   
     var x = document.getElementById("notice_wrapper");
     var xx = document.getElementById("error_wrapper");
    
      if (x){
       x.className = "show";
       setTimeout(function(){ x.className = x.className.replace("show", ""); }, 4500);
      }
      else if (xx){ 
       
         xx.className = "show-me";
        setTimeout(function(){ xx.className = xx.className.replace("show-me", ""); }, 4500);
      }
 });
 
 