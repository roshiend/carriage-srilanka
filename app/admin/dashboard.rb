ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }
  
  content title: proc{ I18n.t("active_admin.dashboard") } do
  columns do
    column do
        panel "Recent Bookings" do
          table_for CartItem.joins(:cart).where('carts.status'=> "purchased", "order_state" => "placed").limit(10) do
            column("Action")   {|cart_item| link_to("View Booking", admin_booking_path(cart_item.id))}
            column("Depature From")   {|cart_item| (cart_item.train_class.train.from_station.name)} 
            column("Arrivie To")   {|cart_item| (cart_item.train_class.train.to_station.name)} 
             column("Total Amount")   {|cart_item| number_to_currency cart_item.total_price}
             column("Order Status")   {|cart_item| status_tag(cart_item.cart.status)}
             column("Delivery Status")   {|cart_item| status_tag(cart_item.cart.delivery_status)}
           # column("Passenger"){|cart_item| (cart_item.passenger.email) }
           
          end
        end
      end
      
      
    end
  end
end
