ActiveAdmin.register CartItem,as: "Bookings" do
 scope :PendingBookings, default: true
    scope :CompletedBookings
    scope :all
   
    
   

    permit_params :order_state,passenger_passports_attributes:[:id , :number, :cart_item_id,:book_ref ]
       actions :all, except: [:new,:destroy, :edit]
      
  
  controller do
    
    def scoped_collection
      #CartItem.joins(:cart,:passenger_passports).where('carts.status'=> "purchased")
      
      CartItem.joins(:cart).where('carts.status'=> "purchased", "carts.delivery_status" => "placed") 
    end
    
    
    
    def show
       @cart_item = CartItem.find(params[:id])
      # @ref = @cart_item.passenger_passports.find(params[:id])
      #@redirect_to = PassengerPassport.bookings.find(params[:id])
       
          if request.patch?
            resource.update_attributes cart_item: params[:order_state] || {}
           # ReceiptMailer.deleivery_mail(cart_item,passenger).deliver_now
            #ReceiptMailer.deleivery_mail(cart_item,passenger).deliver_now
            
          end
          
         
        
    end 
  end
  
  
  
  
  show do 
    
    attributes_table do
    
      row :Depature do |cart_item|
        cart_item.train_class.train.from_station.name 
      end
      
      row :depature_time do |cart_item|
        cart_item.train_class.train.arrival_time.strftime("%H:%m")
      end 
      
      row :Arrive do |cart_item|
        cart_item.train_class.train.to_station.name
      end 
      
      row :arrival_time do |cart_item|
        cart_item.train_class.train.depature_time.strftime("%H:%m")
      end 
      
      row :Train do |cart_item|
        cart_item.train_class.train.train_name
      end 
      
      row :Passengers do |cart_item|
        cart_item.passenger_qty
      end 
      
      row :outbound
      
      row :Total do |cart_item|
        number_to_currency cart_item.total_price
      end
      
      row :PassportNumber do |cart_item|
        ul :class=> 'ppts' do
        cart_item.passenger_passports.each do |ppt|
          li do 
            ppt.number
           
            
            end
        end  
      end
    end
    
   
    
    
        row :BookingRefrences do |cart_item|
        ul :class=> 'ppts' do
        cart_item.passenger_passports.each do |ppt|
          li :name=> ("booking_ref_"+ppt.number) do 
            link_to "Update Refefrence:"+ppt.book_ref,edit_admin_booking_refrence_path(ppt.id)
            
            #(ppt.book_ref)
          end
           
          
        end
        
      end
    end
    
    
    
      row :OrderStatus do |cart_item|
          #cart_item.cart.delivery_status
          status_tag(cart_item.order_state)
          
      end 
      
      
          
      row :updateOrder do |cart_item|
          button_to("Confirm Booking",admin_booking_path(cart_item, cart_item:{order_state: "Deliverd" }),  method: :patch) #Deliverd
           
      end      
    
    end
     active_admin_comments
    end
   
   
    
    # sidebar "Order Confirmations", only: [:show] do
       
       
    #   #render("admin/bookings/book_ref")
    #   # ul do
    #   #   li link_to "updateOrder"
    #   # end 
      
       
        
    # end  

end
