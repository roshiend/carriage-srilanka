ActiveAdmin.register PassengerPassport, as: "BookingRefrences" do
controller do
   
    # def scoped_collection
    #   #CartItem.joins(:cart,:passenger_passports).where('carts.status'=> "purchased")
      
    # # PassengerPassport.where('book_ref'=> "Reference_to_complete") 
    # end
  def update
  
    if update! do 
        return redirect_to admin_booking_path(PassengerPassport.find(params[:id]).cart_item.id)
      end
    end
  end    
    
    
  end
   
   actions :all, except: [:destroy, :show, :new]
  
    permit_params :book_ref
    
   form do |f|
    
    f.inputs " Booking Confrimations " do
      f.input :number, input_html: { disabled: true } 
    end  
     
    f.inputs " BookingReferences " do
      f.input :book_ref, :required => true
    end  
    f.actions 
   end 

end
