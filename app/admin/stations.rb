ActiveAdmin.register Station do
permit_params :name

config.filters = true
filter :name, as: :select, collection: -> { Station.pluck(:name) }


end
