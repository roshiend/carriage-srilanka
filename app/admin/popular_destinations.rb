ActiveAdmin.register PopularDestination do
permit_params :destination_name, :rating, :picture


  form do |f|
    f.inputs " Destinations" do
     
       
      f.input :destination_name
      f.input :picture, :as => :file, 
            :hint =>if f.object.picture.attached?
                      image_tag(popular_destination.thumb)
                    end
    
    end
    f.actions
  end

end
