ActiveAdmin.register Train do
permit_params :arrival_time, :depature_time, :start_station_id, :end_station_id, :end_station_time, :destination, :destination_time, :train_name, :train_number, :train_type , :frequency,train_classes_attributes:[:id , :name,:price]

show do 
    attributes_table do
    
      row :from_station
      row :arrival_time
      row :depature_time
    
      row :to_station
      row :end_station_time
      
      row :destination
      row :destination_time
      
      row :train_name
      row :train_number
      row :train_type
      row :frequency
     
     row :Classes do
        ul :class=> 'Class_list' do
        train.train_classes.each do |t_class|
          li do 
            t_class.name
            t_class.price
              end
          end  
      end
    end
    
    end
    active_admin_comments
  end
  
  index do 
      column :from_station
      column :to_station
      column :destination
      
      column :train_type
      column :frequency
      
      
      
    actions  
    
  end
  

form do |f|
  f.inputs " Journy start from " do
     
      f.collection_select :start_station_id, Station.all, :id, :name, {prompt: "Select start Station"}   
      f.input :arrival_time
      f.input :depature_time
    
  end  
  
  
  
  f.inputs " Journy Ends at " do
      f.collection_select :end_station_id, Station.all, :id, :name, {prompt: "Select End Station"}   
      f.input :end_station_time 
  end
  
  
  f.inputs "Destination " do
    f.input :destination
    f.input :destination_time 
  end  
  
  f.inputs " Train info " do
    f.input :train_name
    f.input :train_number
    f.input :train_type
    f.input :frequency
  end
  
  f.inputs "Train Classes" do
   f.has_many :train_classes do |a|
       
      #a.collection_select :name [:] 
       a.input :name,:hint =>a.object.name
       a.input :price,:hint =>a.object.price
   end
  
  end 

 
 f.actions 
end  

end
