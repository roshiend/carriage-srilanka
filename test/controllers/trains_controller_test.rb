require 'test_helper'

class TrainsControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get trains_home_url
    assert_response :success
  end

  test "should get result" do
    get trains_result_url
    assert_response :success
  end

end
