Rails.application.routes.draw do
  
  
  
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :passengers, :controllers => {:passwords => "passwords" , :registrations => "registrations"} 
    scope '(:locale)', locale:/en|cn/ do 
      root 'trains#home'
      get SecureRandom.urlsafe_base64+'/trains'  => 'trains#result', as: :trains 
      
      resource :train_class , only:[:show],path: SecureRandom.urlsafe_base64+'/train_class'
      
       resource :reservation , only:[:show],path: SecureRandom.urlsafe_base64+'/reservation'
       resource :cart, only: [:show, :create]
       resources :cart_items, only: [:create,:destroy]
       resource :purchases, only:[:show] , as: :purchases do
            collection do
              get :open_orders
              get :completed_orders
              get :all_orders
            end   
          end  
       
       post '/payment' => 'carts#payment', as: :payment 
    end
   
#   #errors path redirection
    match "*path", to: "errors#not_found", via: :all
    match "*path", to: "errors#internal_server_error", via: :all
end
